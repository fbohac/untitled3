
import ReactNative from 'react-native';
import I18n from 'react-native-i18n';

import en from './en.json';
import sk from './sk.json';

I18n.fallbacks = true;

I18n.translations = {
    en,
    sk
};

const currentLocale = I18n.currentLocale();

export const isRTL = currentLocale.indexOf('sk') === 0 || currentLocale.indexOf('en') === 0;

ReactNative.I18nManager.allowRTL(isRTL);

export function strings(name, params = {}) {
    return I18n.t(name, params);
}

export default I18n;

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button} from 'react-native';


import {
    createStackNavigator,
    createAppContainer
} from 'react-navigation';
import welcome from './screens/welcome.js'
import menu from './screens/menu.js'
import priklady from './screens/priklady.js'
import test from './screens/test.js'
import form from './screens/form.js'
import priklad1 from './screens/priklad1.js'
import priklad2 from './screens/priklad2.js'
import test1 from './screens/test1.js'
import test2 from './screens/test2.js'

const RootStack = createStackNavigator({
    welcome: {
        screen: welcome
    },
    menu: {
        screen: menu
    },
    test: {
        screen: test
    },
    priklady: {
        screen: priklady
    },
    form: {
        screen: form
    },
    priklad1: {
        screen: priklad1
    },
    priklad2: {
        screen: priklad2
    },
    test1: {
        screen: test1
    },
    test2: {
        screen: test2
    }

});





const App = createAppContainer(RootStack);

export default App;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
